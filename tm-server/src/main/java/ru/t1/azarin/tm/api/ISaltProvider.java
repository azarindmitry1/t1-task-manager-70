package ru.t1.azarin.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ISaltProvider {

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

}
