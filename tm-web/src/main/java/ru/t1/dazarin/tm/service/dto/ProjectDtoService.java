package ru.t1.dazarin.tm.service.dto;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.dazarin.tm.api.repository.ProjectDtoRepository;
import ru.t1.dazarin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dazarin.tm.model.dto.ProjectDto;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectDtoService {

    private final ProjectDtoRepository projectDtoRepository;

    public List<ProjectDto> findAll(@NotNull final String userId) {
        return projectDtoRepository.findAllByUserId(userId);
    }

    @Nullable
    public ProjectDto findById(@NotNull final String userId, @NotNull final String id) {
        return projectDtoRepository.findByUserIdAndId(userId, id);
    }

    public ProjectDto create(@NotNull final String userId) {
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setName("Project Name " + System.currentTimeMillis());
        projectDto.setDescription("Project Description");
        projectDto.setUserId(userId);
        return projectDtoRepository.saveAndFlush(projectDto);
    }

    public void deleteById(@NotNull final String userId, @NotNull final String projectDtoId) {
        if (!projectDtoRepository.existsById(projectDtoId)) throw new ProjectNotFoundException();
        projectDtoRepository.deleteByUserIdAndId(userId, projectDtoId);
    }

    public void deleteAll(@NotNull final String userId) {
        projectDtoRepository.deleteByUserId(userId);
    }

    public ProjectDto save(@NotNull final String userId, @NotNull final ProjectDto projectDto) {
        projectDto.setUserId(userId);
        return projectDtoRepository.save(projectDto);
    }

}