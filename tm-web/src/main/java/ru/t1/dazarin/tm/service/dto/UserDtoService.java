package ru.t1.dazarin.tm.service.dto;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.t1.dazarin.tm.api.repository.UserDtoRepository;
import ru.t1.dazarin.tm.enumerated.RoleType;
import ru.t1.dazarin.tm.exception.entity.UserNotFoundException;
import ru.t1.dazarin.tm.exception.field.LoginEmptyException;
import ru.t1.dazarin.tm.model.dto.RoleDto;
import ru.t1.dazarin.tm.model.dto.UserDto;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Collections;

@Service
@RequiredArgsConstructor
public class UserDtoService {

    private final UserDtoRepository userDtoRepository;

    private final PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMINISTRATOR);
        initUser("user", "user", RoleType.USER);
    }

    private void initUser(final String login, final String password, final RoleType roleType) {
        final UserDto user = userDtoRepository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @Transactional
    public void createUser(final String login, final String password, final RoleType roleType) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        final String passwordHash = passwordEncoder.encode(password);
        final UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        final RoleDto role = new RoleDto();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userDtoRepository.save(user);
    }

    public UserDto findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto user = userDtoRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

}
