package ru.t1.dazarin.tm;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dazarin.tm.client.ProjectClient;
import ru.t1.dazarin.tm.marker.IntegrationCategory;
import ru.t1.dazarin.tm.model.dto.ProjectDto;

@Category(IntegrationCategory.class)
public class ProjectClientTest {

    private static final String BASEURL = "http://localhost:8080/api/project";

    private final ProjectClient projectClient = ProjectClient.client(BASEURL);

    private ProjectDto projectOne;

    private ProjectDto projectTwo;

    @Before
    public void setUp() {
        projectOne = projectClient.create();
        projectTwo = projectClient.create();
    }

    @After
    public void tearDown() {
        projectClient.deleteAll();
    }

    @Test
    public void findAll() {
        Assert.assertEquals(2, projectClient.findAll().size());
    }

    @Test
    public void findById() {
        final ProjectDto projectDto = projectClient.findById(projectOne.getId());
        Assert.assertEquals(projectOne.getId(), projectDto.getId());
    }

    @Test
    public void create() {
        final ProjectDto projectDto = projectClient.create();
        Assert.assertEquals(projectDto.getId(), projectClient.findById(projectDto.getId()).getId());
    }

    @Test
    public void deleteById() {
        projectClient.deleteById(projectOne.getId());
        Assert.assertEquals(1, projectClient.findAll().size());
    }

    @Test
    public void deleteAll() {
        projectClient.deleteAll();
        Assert.assertEquals(0, projectClient.findAll().size());
    }

    @Test
    public void update() {
        final ProjectDto projectDto = projectClient.findById(projectOne.getId());
        projectDto.setDescription("test update");
        projectClient.update(projectDto);
        Assert.assertEquals("test update", projectClient.findById(projectDto.getId()).getDescription());
    }

}
